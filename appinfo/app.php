<?php
namespace OCA\GroupAlert;

use OCP\Util;
use OCP\AppFramework\Http\EmptyContentSecurityPolicy;

Util::addStyle('groupalert', 'alert-modal');


// only load when the file app displays
$eventDispatcher = \OC::$server->getEventDispatcher();

$eventDispatcher->addListener(
    'OCA\Files::loadAdditionalScripts',
    function() {
        $userSession = \OC::$server->getUserSession();

        if ($userSession->isLoggedIn()) {
            Util::addScript('groupalert', 'alert-modal');
        }
    }

);

$policy = new EmptyContentSecurityPolicy();
$policy->addAllowedImageDomain('*');
$policy->addAllowedMediaDomain('*');

\OC::$server->getContentSecurityPolicyManager()->addDefaultPolicy($policy);