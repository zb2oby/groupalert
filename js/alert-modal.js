function getTargetDir() {
    var targetDir = '/';
    var currentUrl = window.location.href;
    try {
        var targetDir = currentUrl.split('?')[1].split('&')[0];
        if (targetDir.split('/')[0] === 'dir=') {
            targetDir = targetDir.split('=')[1];
        }
    }
    catch(err) {

    }
    return targetDir;
}

function checkAlreadyViewedAndUpdateCookie(messageId) {

    var viewedMessages = [];
    var alreadyViewed = false;

    if (cookied === "1") {
        if (sessionStorage.getItem("viewed") === null) {
            viewedMessages.push(messageId);
            //set a session cookie to save alert status for one day
            sessionStorage.setItem("viewed", JSON.stringify(viewedMessages));
        } else {
            //if session cookie is set, with message id, do not show message
            viewedMessages = JSON.parse(sessionStorage.getItem("viewed"));

            if (viewedMessages.includes(messageId)) {
                alreadyViewed = true;
            } else {
                //if session cookie is set, without message id, add message id to the cookie
                viewedMessages.push(messageId);
                sessionStorage.setItem("viewed", JSON.stringify(viewedMessages));
            }
        }
    }
    return alreadyViewed;
}

function showMessage() {
    var targetDir = decodeURIComponent(getTargetDir());

    $.post(OC.generateUrl('/apps/groupalert/display/message'), {targetDir: targetDir}, function (response) {

        if (response.display === 'true') {

            if (!checkAlreadyViewedAndUpdateCookie(response.id, response.cookied)) {
                $('.GA-message-content').html(response.text.replace('<p><br></p>',''));
                $('.GA-message').fadeIn(450, function () {

                });
                $('#cboxOverlay').show();
                $('#cboxOverlay').css({
                    'opacity': '0.4',
                    'cursor': 'pointer',
                    'visibility': 'visible',
                    'display': 'block'
                });
            }

        }
    });
}

$(document).ready(function () {

    //add HTML to files view
    $('#app-content-files').append('<div class="GA-message" style="display: block;">\n' +
        '\t<div class="GA-close"><img class="svg" src="../../../core/img/actions/close.svg" alt="x"></div>\n' +
        '\t<div class="GA-message-content"></div>\n' +
        '</div>');

    //By default hide HTML
    $('.GA-message').hide();
    //display message on document ready
    showMessage();
    //display on click on filelist dir & breadcrumb
    $(document).on('click', 'a.name, .crumb', showMessage);

    $('.GA-close').click(function() {
        $('.GA-message').fadeOut(450, function(){

        });

        $('#cboxOverlay').fadeOut(450, function(){
        });
    });

});